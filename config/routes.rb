Rails.application.routes.draw do
  # get 'friends/show'

  devise_for :users
  get '/signup', to: 'users#sign_up'
  get '/login', to: 'users#sign_in'

  resources :posts do
    resources :comments
  end

  resources :friends, only: [] do
    collection do
      get :add_friend
      get :accept_friend
    end
  end

  resources :friends

  authenticated :user do
    root "dashboard#show"
  end

   unauthenticated do
    root 'home#index'
  end

end
