module PostsHelper
  def check_comment_owner comment, user
    comment.user_id == user.id
  end
end
