class Friendship < ApplicationRecord
  # status - requested, accepted, rejected
  enum status: [ :requested, :accepted ]

  belongs_to :user
  validates :user_id, presence: true
  validates :friend_id, presence: true

  def self.check_friendship user_1, user_2
    friend = [false, nil]
    friendship = user_1.friendships.where(friend_id: user_2).first

    if friendship
      friend = [true, friendship]
    else
      friendship = Friendship.where(user_id: user_2, friend_id: user_1).first
      if friendship
        friend = [true, friendship]
      end
    end
    friend
  end
end
