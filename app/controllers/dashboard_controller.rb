class DashboardController < ApplicationController

  def new
    @user= User.new
  end

  def show
      #@user= User.find(params[:id])
    @posts = current_user.posts
    @users = User.all
  end

end
