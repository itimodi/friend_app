class PostsController < ApplicationController
	before_action :set_post, only: [:show, :edit, :update, :destroy]

	def index
		@posts =Post.all
	end

	def show
		@post= Post.find(params[:id])
    @comment = Comment.new
    # @user = @post.find(params[:user_id])
	end

	def new
		@post =Post.new
	end

	def edit
		@post = Post.find(params[:id])
	end

	def create
		@post = current_user.posts.build(post_params)
		if @post.save
			flash[:success] = "Post created!"
            redirect_to :action => 'show', :id => @post
        end
	end

	def update
		@post = Post.find(params[:id])

		if @post.update_attributes(post_params)
			redirect_to :action => 'show', :id => @post
		end
	end

	def destroy
		@post.destroy
		flash[:success] = "Post deleted"
		redirect_to root_url
    end

    private

    def set_post
      @post = Post.find(params[:id])
    end

    def post_params
      params.require(:post).permit(:content, :title)
    end

end
