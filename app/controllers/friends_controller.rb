class FriendsController < ApplicationController

  # def new
  #   # @friend = Friendship.new
  # end 

  def add_friend
    # columns = user_id, friend_id, status
    # params = friend_id

    #  current_user as user
    # friendship = current_user.friendships.where(friend_id: params[:friend_id]).first

    # unless friendship
    #   # current_user as friend_id
    #   friendship = Friendship.where(user_id: params[:friend_id], friend_id: current_user.id).first
    #   unless friendship
    #     friendship = current_user.friendships.where(friend_id: params[:friend_id]).create
    #   end
    # end

    user_2 = User.find(params[:friend_id])
    friends = Friendship.check_friendship current_user, user_2
    unless friends[0]
      friends[1] = current_user.friendships.where(friend_id: params[:friend_id]).create
    end
    friends[1].requested!
    redirect_to root_path

    # @user= User.find(params[:user_id])
    # @friend= @user.friend.new(user_id: current_user.id, friend_id: user_id, friendship: false)
    #current_user.friendships.build()


  end

  def destroy
    user_2 = User.find(params[:id])
    friends = Friendship.check_friendship current_user, user_2
    #@friend = current_user.friendships.find(params[:friend_id])
    friends[1].destroy
    flash[:success] = "Request rejected"
    redirect_to root_url
  end

  def accept_friend
    user_2 = User.find(params[:friend_id])
    friends = Friendship.check_friendship current_user, user_2
    # if friends.update_attributes(friend_params)
    friends[1].accepted!
    redirect_to root_path
    # end

  end

  # def edit

  # end

  # def update
  # end
  private

  def friend_params
    #params.require(:friendship).permit(:status=1)
  end

end
